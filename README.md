# Ubiquiti EdgeRouter X configuration

## Features

1. Internet connection
2. 2 separate LANs that only can access Internet
3. Support for Apple AirPort Extreme guest network that only can access Internet
4. IPv6 with 6RD and dynamic IPv4 address
5. Dynamic DNS with DuckDNS
6. External access to server with port forwarding with hairpin NAT
7. HTTPS certificate from Let's Encrypt

## Before you start:

* Set a new password for the admin (ubnt) user.
* Upgrade to the latest image from https://www.ubnt.com/download/edgemax/

## Configure

The configuration is based on the WAN+2LAN2 wizard, but all steps from the wizard is included below.

### WAN (Internet)

    set interfaces ethernet eth0 address dhcp
    set interfaces ethernet eth0 description Internet
        
    set service nat rule 5010 description "masquerade for WAN"
    set service nat rule 5010 outbound-interface eth0
    set service nat rule 5010 type masquerade

### Separate LANs

I was already using these blocks for the main LAN and guest LAN, but you can pick any private IPv4 addresses you like.
Just make sure to change the corresponding DHCP and firewall settings.

* One guest LAN (192.168.10.0/24) on eth1
* One main LAN (192.168.1.0./24) on eth2-eth4 

<!-- well hey there sailor -->

    set interfaces ethernet eth1 address 192.168.10.1/24
    set interfaces ethernet eth1 description "Guest LAN"
    set interfaces switch switch0 address 192.168.1.1/24
    set interfaces switch switch0 description "Main LAN"
    set interfaces switch switch0 switch-port interface eth2
    set interfaces switch switch0 switch-port interface eth3
    set interfaces switch switch0 switch-port interface eth4
    set interfaces switch switch0 switch-port mtu 1500

#### Limit Guest LAN bandwith

Limit the bandwidth of the guest LAN port to 10 Mbps, as it is only used to connect to Internet.

    set ethernet eth1 bandwidth maximum 10m

### Apple AirPort Extreme guest network

Since the ERX does all the routing, the Apple AirPort Extreme is set to bridge mode. However, it still supports
a guest WiFi network. It tags traffic from the guest network on VLAN 1003. Hence, we need to add a VLAN to the switch0
group (eth2-eth4):

    set interfaces switch switch0 vif 1003 address 192.168.3.1/24
    set interfaces switch switch0 vif 1003 description "AirPort guest net"
    set interfaces switch switch0 switch-port vif 1003 mtu 1500

### DCHP  

* Guest LAN (LAN1): 192.168.10.0/24
* Main LAN (LAN2): 192.168.1.0/24 - reserve 2-9 for static IPs
* AirPort guest net (GUEST): 192.168.3.0/24

<!-- well hey there sailor -->

    set service dhcp server 
    set service dhcp server disabled false
    set service dhcp server hostfile-update disable
    set service dhcp server use-dnsmasq disable

    set service dhcp server shared-network-name LAN1 authoritative disable
    set service dhcp server shared-network-name LAN1 subnet 192.168.10.0/24 default-router 192.168.10.1
    set service dhcp server shared-network-name LAN1 subnet 192.168.10.0/24 dns-server 192.168.10.1
    set service dhcp server shared-network-name LAN1 subnet 192.168.10.0/24 lease 86400
    set service dhcp server shared-network-name LAN1 subnet 192.168.10.0/24 start 192.168.10.2 stop 192.168.10.254

    set service dhcp server shared-network-name LAN2 authoritative disable
    set service dhcp server shared-network-name LAN2 subnet 192.168.1.0/24 default-router 192.168.1.1
    set service dhcp server shared-network-name LAN2 subnet 192.168.1.0/24 dns-server 192.168.1.1
    set service dhcp server shared-network-name LAN2 subnet 192.168.1.0/24 lease 86400
    set service dhcp server shared-network-name LAN2 subnet 192.168.1.0/24 start 192.168.1.10 stop 192.168.1.254

    set service dhcp server shared-network-name GUEST authoritative disable
    set service dhcp server shared-network-name GUEST subnet 192.168.3.0/24 default-router 192.168.3.1
    set service dhcp server shared-network-name GUEST subnet 192.168.3.0/24 dns-server 192.168.3.1
    set service dhcp server shared-network-name GUEST subnet 192.168.3.0/24 lease 86400
    set service dhcp server shared-network-name GUEST subnet 192.168.3.0/24 start 192.168.3.2 stop 192.168.3.254


### DNS

Get DNS servers for forwarding from DHCP on eth0, increase cache to 1000, and forward DNS requests for all LANs

    set service dns forwarding dhcp eth0
    set service dns forwarding cache-size 1000
    set service dns forwarding listen-on eth1
    set service dns forwarding listen-on switch0
    set service dns forwarding listen-on switch0.1003

### Clock

Enable NTP and set local time zone

    set system ntp server 0.ubnt.pool.ntp.org
    set system ntp server 1.ubnt.pool.ntp.org
    set system ntp server 2.ubnt.pool.ntp.org
    set system ntp server 3.ubnt.pool.ntp.org
    set sytem time-zone Europe/Oslo

### Firewall

Generic firewall settings. Some of these may be default values - I haven't checked yet:

    set firewall all-ping enable
    set firewall broadcast-ping disable
    set firewall ipv6-receive-redirects disable
    set firewall ipv6-src-route disable
    set firewall ip-src-route disable
    set firewall log-martians enable
    set firewall receive-redirects disable
    set firewall send-redirects enable
    set firewall source-validation disable
    set firewall syn-cookies enable


#### Internet (WAN)

Block all incoming connections. WAN_IN is important when IPv6 is enabled, as IPv6 is not behind NAT.
WAN_LOCAL is to block managing the router from the Internet.

    set firewall name WAN_IN
    set firewall name WAN_IN default-action drop
    set firewall name WAN_IN description "WAN to internal"
    set firewall name WAN_IN rule 10 action accept
    set firewall name WAN_IN rule 10 description "Allow established/related"
    set firewall name WAN_IN rule 10 state established enable
    set firewall name WAN_IN rule 10 state related enable
    set firewall name WAN_IN rule 20 action drop
    set firewall name WAN_IN rule 20 description "Drop invalid state"
    set firewall name WAN_IN rule 20 state invalid enable
    
    set firewall name WAN_LOCAL
    set firewall name WAN_LOCAL default-action drop
    set firewall name WAN_LOCAL description "WAN to router"
    set firewall name WAN_LOCAL rule 10 action accept
    set firewall name WAN_LOCAL rule 10 description "Allow established/related"
    set firewall name WAN_LOCAL rule 10 state established enable
    set firewall name WAN_LOCAL rule 10 state related enable
    set firewall name WAN_LOCAL rule 20 action drop
    set firewall name WAN_LOCAL rule 20 description "Drop invalid state"
    set firewall name WAN_LOCAL rule 20 state invalid enable
    
    set interfaces ethernet eth0 firewall in name WAN_IN
    set interfaces ethernet eth0 firewall local name WAN_LOCAL

#### Separating the LANs

I wanted the three LANs to be unable to see each other. Firewalling is done on the IP level, so the same rules can
be applied to all three nets, with one exception - the ESX must be reachable from my main LAN on switch0 for
management.

Create a network group:

    set firewall group network-group LAN_NETWORKS
    set firewall group network-group LAN_NETWORKS description "LAN Networks"
    set firewall group network-group LAN_NETWORKS network 192.168.1.0/24
    set firewall group network-group LAN_NETWORKS network 192.168.3.0/24
    set firewall group network-group LAN_NETWORKS network 192.168.10.0/24
    commit

Create rules to protect incoming traffic for the LANs. We only want to allow outgoing connections, everything else
should be dropped:

    set firewall name PROTECT_IN 
    set firewall name PROTECT_IN rule 10 action accept
    set firewall name PROTECT_IN rule 10 description "Accept Established/Related"
    set firewall name PROTECT_IN rule 10 protocol all
    set firewall name PROTECT_IN rule 10 state established enable
    set firewall name PROTECT_IN rule 10 state related enable
    set firewall name PROTECT_IN rule 30 action drop
    set firewall name PROTECT_IN rule 30 description "Drop LAN_NETWORKS"
    set firewall name PROTECT_IN rule 30 destination group network-group LAN_NETWORKS
    set firewall name PROTECT_IN rule 30 protocol all
    commit
    
Create rules to protect ESX from the guest networks. We only want to allow DNS and DHCP:

    set firewall name PROTECT_LOCAL 
    set firewall name PROTECT_LOCAL default-action drop 
    set firewall name PROTECT_LOCAL rule 10 action accept
    set firewall name PROTECT_LOCAL rule 10 description "Accept DNS"
    set firewall name PROTECT_LOCAL rule 10 destination port 53
    set firewall name PROTECT_LOCAL rule 10 protocol udp
    set firewall name PROTECT_LOCAL rule 20 action accept
    set firewall name PROTECT_LOCAL rule 20 description "Accept DHCP"
    set firewall name PROTECT_LOCAL rule 20 destination port 67
    set firewall name PROTECT_LOCAL rule 20 protocol udp
    commit
   
Apply rules. Note that PROTECT_LOCAL **not** is applied to switch0:

    set interfaces ethernet eth1 firewall in name PROTECT_IN
    set interfaces ethernet eth1 firewall local name PROTECT_LOCAL
    set interfaces ethernet switch0 firewall in name PROTECT_IN
    set interfaces ethernet switch0 vif 1003 firewall in name PROTECT_IN
    set interfaces ethernet switch0 vif 1003 firewall local name PROTECT_LOCAL
    commit
    

## 6RD with dynamic IPv4 address

Currently only the main LAN has IPv6 support. If providing IPv6 on the guest LANs, IPv6 version of the PROTECT_IN
and PROTECT_LOCAL rules must be applied as well.


### Update 6RD automatically if IPv4 address changes

Place [ipv6-6rd-update.sh](ipv6-6rd-update.sh) in /config/scripts

    scp ipv6-6rd-update.sh ubnt@192.168.1.1:/config/scripts

Run it every 5 minute on ERX:

    configure
    set system task-scheduler task ipv6-6rd-update executable path /config/scripts/ipv6-6rd-update.sh
    set system task-scheduler task ipv6-6rd-update interval 5m
    commit
    save
    exit

### Dynamic DNS with DuckDNS

#### Prerequisites:

* Register an account and hostname on https://www.duckdns.org/

#### Configuration

    configure
    set service dns dynamic interface eth0 service custom-duckdns
    set service dns dynamic interface eth0 service custom-duckdns host-name your-hostname-here
    set service dns dynamic interface eth0 service custom-duckdns login nouser
    set service dns dynamic interface eth0 service custom-duckdns password your-token-here
    set service dns dynamic interface eth0 service custom-duckdns protocol dyndns2
    set service dns dynamic interface eth0 service custom-duckdns server www.duckdns.org
    commit
    save
    exit
    
#### Manual update

    update dns dynamic interface eth0

#### Status

   show dns dynamic status

### External access to server with port forwarding with hairpin NAT

Running a server on one LAN, accessible from Internet and all LANs. We want to be able to access
the server using the hostname and external IP also from the LANs. This is called "hairpin NAT".
First we must make an opening in the `PROTECT_IN` rule, and then we add port forwarding from
the WAN interface. The `PROTECT_IN` rule must be added before the drop rule.

Note that the `PROTECT_IN` rule must be added even for hairpin access from the same LAN as the
server. In addition, all LAN interfaces we want do to hairpin access from must be added to the
port forwarding, not only the LAN that contains the server.

    configure

    set firewall name PROTECT_IN rule 20 action accept
    set firewall name PROTECT_IN rule 20 description 'Allow to web server'
    set firewall name PROTECT_IN rule 20 destination address 192.168.1.2
    set firewall name PROTECT_IN rule 20 destination port 443
    set firewall name PROTECT_IN rule 20 log disable
    set firewall name PROTECT_IN rule 20 protocol tcp
    set port-forward auto-firewall enable
    set port-forward hairpin-nat enable
    set port-forward wan-interface eth0
    set port-forward lan-interface switch0
    set port-forward lan-interface switch0.1003
    set port-forward lan-interface eth1
    set port-forward rule 1 description Web server
    set port-forward rule 1 forward-to address 192.168.1.2
    set port-forward rule 1 forward-to port 443
    set port-forward rule 1 original-port 443
    set port-forward rule 1 protocol tcp
    
    commit
    save
    exit

### HTTPS certificate from Let's Encrypt

1. Set DNS CNAME of your own domain to point to Duck DNS hostname. Commands below assumes
   `subdomain.example.com` is an alias for `yourdomain.duckdns.org`.
2. Log into Edge router.
   ```
   curl https://raw.githubusercontent.com/j-c-m/ubnt-letsencrypt/master/install.sh | sudo bash
   sudo /config/scripts/renew.acme.sh -d subdomain.example.com
   configure
   set system static-host-mapping host-name subdomain.example.com inet 192.168.1.1
   set service gui cert-file /config/ssl/server.pem
   set service gui ca-file /config/ssl/ca.pem
   set system task-scheduler task renew.acme executable path /config/scripts/renew.acme.sh
   set system task-scheduler task renew.acme interval 1d
   set system task-scheduler task renew.acme executable arguments '-d subdomain.example.com'
   commit
   save
   exit
   ```

## Sources:
* EdgeRouter - Add VLANs to switch interface
  * https://help.ubnt.com/hc/en-us/articles/204975914-EdgeRouter-Add-VLANs-to-switch-interface
* EdgeRouter - How to Protect a Guest Network on EdgeRouter:
  * https://help.ubnt.com/hc/en-us/articles/218889067-EdgeRouter-How-to-Protect-a-Guest-Network-on-EdgeRouter
* EdgeRouter X - Config for Split into 4 Subnets (1 per Port):
  * https://community.ubnt.com/t5/EdgeMAX/EdgeRouter-X-Config-for-Split-into-4-Subnets-1-per-Port/td-p/1662997
* AirPort Extreme guest network in bridge mode:
  * https://www.thegeekpub.com/5191/use-airport-extreme-guest-network-bridge-mode/
  * https://lucatnt.com/2016/05/enable-guest-network-on-airport-basestations-in-bridge-mode/
* Altibox 6RD configuration:
  * https://www.altibox.no/privat/bredband/ipv6/
* 6RD:
  * https://community.ubnt.com/t5/EdgeMAX/IPv6-6rd-setup-configuration-writeup/td-p/2052351
* Update 6RD with dynamic IPv4 address:
  * https://community.ubnt.com/t5/EdgeMAX/IPv6-6rd-with-Dynamic-IPv4-address-from-ISP-cron-script-to/m-p/1590246
* DuckDNS on EdgeRouter
  * https://loganmarchione.com/2017/04/duckdns-on-edgerouter/
* EdgeRouter - Port Forwarding
  * https://help.ui.com/hc/en-us/articles/217367937-EdgeRouter-Port-Forwarding
* Let's Encrypt with the Ubiquiti EdgeRouter
  * https://github.com/j-c-m/ubnt-letsencrypt