#!/bin/vbash

# Device specific variables
wanDev="eth0"
lanType="switch"
lanDev="switch0"
tunDev="tun0"

# ISP specific variables
IPv6Prefix=2a01:79c::
IPv6PrefixNumber=0x2a01079c00000000 # <-- Pad all groups with 0 to fill 64 bits (16 characters), remove : and prefix with 0x
                                    # 2a01:79c::
                                    # 2a01:079c:0000:0000
                                    # 2a01079c00000000
                                    # 0x2a01079c00000000
                                    # This can be scripted, but enough bit fiddling is done below...
IPv6PrefixLength=30

wanIPv4=$(ip addr list $wanDev | grep 'inet ' | awk '{print $2}' | cut -d/ -f1)
wanIPv6Old=$(ip addr list $lanDev | grep 'inet6 ' | grep -v link | awk '{print $2}' | cut -d/ -f1)

# Merge (OR) prefix with IPv4 - bit shift IPv4 address if prefix is less length than 32 bits before merging
wanIPv6New=$(printf "%016x" $(($IPv6PrefixNumber | ($(printf "0x%02x%02x%02x%02x" $(echo $wanIPv4 | tr . ' ')) << (32 - $IPv6PrefixLength)))))
# Format as compressed IPv6 address - should use sipcalc here
delegatedPrefix=$(printf "%x" "0x"${wanIPv6New:0:4}):$(printf "%x" "0x"${wanIPv6New:4:4}):$(printf "%x" "0x"${wanIPv6New:8:4}):$(printf "%x" "0x"${wanIPv6New:12:4})::
wanIPv6New=${delegatedPrefix}1

if [ -z "$wanIPv4" ] || [ "$wanIPv6Old" == "$wanIPv6New" ]; then
	exit
fi

source /opt/vyatta/etc/functions/script-template

configure

delete interfaces $lanType $lanDev address "$wanIPv6Old/64"
delete interfaces $lanType $lanDev ipv6 router-advert prefix

set interfaces $lanType $lanDev address "$wanIPv6New/64"

set interfaces $lanType $lanDev ipv6 router-advert prefix "$delegatedPrefix/64" autonomous-flag true
set interfaces $lanType $lanDev ipv6 router-advert prefix "$delegatedPrefix/64" on-link-flag true
set interfaces $lanType $lanDev ipv6 router-advert prefix "$delegatedPrefix/64" valid-lifetime 2592000

set interfaces tunnel $tunDev disable
delete interfaces tunnel $tunDev address "$wanIPv6Old/$IPv6PrefixLength"
set interfaces tunnel $tunDev address "$wanIPv6New/$IPv6PrefixLength"
set interfaces tunnel $tunDev local-ip $wanIPv4
delete interfaces tunnel $tunDev disable
commit
save
exit
